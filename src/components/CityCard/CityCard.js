import React from 'react';
import classes from './CityCard.module.css'

const CityCard = ({data}) => {

    const {name, sys:{country}, main:{temp, feels_like, humidity}, weather } = data
        
    return (
        <div className={classes.CityCard}>
            <h2 className={classes.title}>
                <span className={classes.name}>{name}</span>
                <sup className={classes.sup}> {country} </sup>
            </h2>

            <div className={classes.temp}>
                {Math.round(temp)}
                <sup> &deg;C</sup>
            </div>
                <p>Feels like: {Math.round(feels_like)} <sup> &deg;C</sup> </p>

            <div className={classes.info}>
                <img className={classes.icon} src={`https://openweathermap.org/img/wn/${weather[0].icon}@2x.png`} alt={weather[0].description}  />
                <p> {weather[0].description} </p>
                
            </div>
                <p className={classes.humidity}>Humidity: {humidity}% </p>
        </div>
    )
}

export default CityCard;
