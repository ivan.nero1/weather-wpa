import React from 'react';
import classes from './Input.module.css';

const Input = ({query, onSearch, onKeyPress}) => {
    return (
        <input
            className={classes.search}
            type="text"
            placeholder="Enter the city"
            value={query}
            onChange={e => onSearch(e.target.value)}
            onKeyPress={onKeyPress} />
    );
}

export default Input;
