import React, {useState} from 'react';
import {fetchWeather} from './api/fetchWeather'
import './App.css'
import CityCard from './components/CityCard/CityCard'
import Input from './components/Input/Input'

const App = () => {

    const [ query, setQuery ] = useState("")
    const [ weather, setWeather ] = useState({})
    
    const search = async (e) => {
        if (e.key === 'Enter') {
            const data = await fetchWeather(query)
            console.log(data)
            setWeather(data)
            setQuery('')
        }
    }
    
    return (
        <div className="main-container">
            <h1>Weather PWA App</h1>
            <Input 
            query={query}
            onKeyPress={search}
            onSearch={ e => setQuery(e)}
             />

            { weather.sys && <CityCard data={weather} /> }
        </div>
    );
}

export default App;
